export default class BasePage {
  constructor(protected path: string) {
    this.navigateTo(path);
  }

  public async navigateTo(path: string): Promise<void> {
    await browser.url(path);
  }
}
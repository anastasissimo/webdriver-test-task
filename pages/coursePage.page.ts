import BasePage from './base.page';
import {Service} from "typedi";

let assert = require('chai').assert;

@Service()
class CoursePage extends BasePage{

  constructor(){
    super('/courses')
  }
  
  showHideLessonsButton: WebdriverIO.Element
  courseTag : WebdriverIO.Element
  lessonsCounterButton : WebdriverIO.Element
  topicButton : WebdriverIO.Element
  lessonsBlock : WebdriverIO.Element;

  get showHideLessonsButtonElement(): WebdriverIO.Element {
    this.showHideLessonsButton = $('.showHideButton');
    return this.showHideLessonsButton;
  }

  get coursseTagElement(): WebdriverIO.Element {
    this.courseTag = $('#courseTag');
    return this.courseTag;
  }

  get topicButtonElement(): WebdriverIO.Element {
    this.topicButton = $('.topicButton');
    return this.topicButton;
  }

  get lessonsBlockElement(): WebdriverIO.Element {
    this.lessonsBlock = $('.lessonsBlock');
    return this.lessonsBlock;
  }
  get lessonsCounterButtonElement(): WebdriverIO.Element {
    this.lessonsCounterButton = $('.lessonsCounterButton');
    return this.lessonsCounterButton;
  }

  checkLessonsCount(counter: number) {
    const lessonsAmount = Number(this.lessonsCounterButtonElement.getText().substr(0,1));
    assert.strictEqual(lessonsAmount, counter);
    assert(lessonsAmount > 0);
    return this
  }

  checkLessonsListShown() {
    assert(this.lessonsBlockElement.isDisplayed());
    return this
  }

  checkShowHideButtonListShown() {
    this.showHideLessonsButtonElement.click();
    assert(!this.lessonsBlockElement.isDisplayed());
    this.showHideLessonsButtonElement.click();
    assert(this.lessonsBlockElement.isDisplayed());
    return this
  }


  checkCourseTag(courseName: string) {
    assert.strictEqual(this.coursseTagElement.getText(), courseName);
    return this
  }

  checkTopicButton(topic: string) {
    assert.strictEqual(this.topicButtonElement.getText(), topic);
    return this
  }
}

export default CoursePage;
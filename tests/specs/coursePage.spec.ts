import CoursePage from '../../pages/coursePage.page' ;

describe('Lessons Page', () => {
  it('Testing things', async() => {
    const coursePage = new CoursePage();
    coursePage.navigateTo('/courses');
    coursePage
      .checkLessonsCount(3)
      .checkLessonsListShown()
      .checkTopicButton('Topic Name')
      .checkCourseTag('Course name')
      .checkShowHideButtonListShown()
  })
});